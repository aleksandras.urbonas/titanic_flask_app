######################################################## define

import pandas as pd
import pickle as pkl

from flask import Flask, request

from wtforms import Form, StringField, validators


######################################################## create app

app = Flask(__name__)

model = pkl.load('model/model.pkl')
    

######################################################## form

class XForm(Form):
    """
    inherit from request.form
    """
    
    age = StringField(
        'Age (for example, 25):'
        , validators=[validators.input_required()]
    )



######################################################## define pages

@app.route('/')
def home():
    return "<a href='/train'>train</a>"


######################################################## train page

@app.route('/train')
def train():
    df = pd.read_csv('data/titanic.csv')
    
    # model
    
#     return f"<h1>Rows loaded: {df.shape[0]}</h1>"  # v1
    return "Model trained!"  # v2 = Final


######################################################## predict

@app.route('/predict')
def predict():
    
    
    form = XForm(request.form)
    
    
    if request.method == 'POST' and form.validate():
        age = form.age.data
    
    
    return "<h1>Titanic: predict</h1>"


######################################################## main start

if __name__ == "__main__":
    app.run()

######################################################## main end