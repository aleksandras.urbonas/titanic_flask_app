print("Loading {forms_predictors}")


from wtforms import \
    Form \
    , TextAreaField \
    , validators \
    , StringField \
    , SubmitField
    #, TextField -- deprecated


class PredictorsForm(Form):
	"""
	class for retrieving user's input through form

	Inherits: request.form class
	"""
	p_class = StringField(
        u'P Class (Valid Values: 1, 2, 3)'
        , validators=[validators.input_required()]
    )
    
	sex = StringField(
        u'Sex (0: Female and 1: Male)'
        , validators=[validators.input_required()]
    )
    
	age = StringField(
        u'Age (For eg.: 24)'
        , validators=[validators.input_required()]
    )
    
	sibsp = StringField(
        u'Siblings and Spouse Count (For eg.: 3)'
        , validators=[validators.input_required()]
    )
    
	parch = StringField(
        u'Parch (Valid Values: 0, 1, 2, 3, 4, 5, 6)'
        , validators=[validators.input_required()]
    )
    
	fare = StringField(
        u'Fare (For eg.: 100)'
        , validators=[validators.input_required()]
    )
    
	embarked = StringField(
        u'Embarked (Valid Values: 0, 1, 2)'
        , validators=[validators.input_required()]
    )


if __name__ == "__main__":
    print("{forms_predictors.py} loaded!")
