import joblib
import pandas as pd

import os
import shutil
import sys
import time
import traceback


from flask import Flask, request, jsonify

# from sklearn.externals import joblib


### create new app
app = Flask(__name__)


### get data
DATA_DIR = 'data'
training_data = os.path.join(DATA_DIR, 'titanic.csv')
include = ['Age', 'Sex', 'Embarked', 'Survived']
dependent_variable = include[-1]


### model
MODEL_DIR = 'models'
model_file_path = os.path.join(MODEL_DIR, 'model.pkl')
model_columns_file_path = os.path.join(MODEL_DIR, 'model_columns.pkl')

### These will be populated at training time
model_columns = None
clf = None


@app.route('/predict', methods=['POST'])
def predict():
    if clf:
        try:
            json_ = request.json
            query = pd.get_dummies(pd.DataFrame(json_))

            # https://github.com/amirziai/sklearnflask/issues/3 
            query = query.reindex(columns=model_columns, fill_value=0)

            prediction = list(clf.predict(query))

            return jsonify({'prediction': prediction})
        except Exception as e:
            return jsonify({'error': str(e), 'trace': traceback.format_exc()})
    else:
        print('train first')
        return 'no model here'


@app.route('/train', methods=['GET'])
def train():
    # using random forest as an example
    # can do the training separately and just update the pickles

    from sklearn.ensemble import RandomForestClassifier


    df = pd.read_csv(training_data)
    
    
    ### include chosen variables
    df_ = df.copy()
    df_ = df_[include]


    #### DATA PREPARATION ####
    
    ### encode categorical variables with `one-hot`
    categoricals = []

    for col, col_type in df_.dtypes.iteritems():
        if col_type == 'O':
            categoricals.append(col)
        else:
            df_[col].fillna(0, inplace=True)  # fill NA's with 0 for ints/floats, too generic


    ### get_dummies effectively creates one-hot encoded variables
    df_ohe = pd.get_dummies(df_, columns=categoricals, dummy_na=True)

    x = df_ohe[df_ohe.columns.difference([dependent_variable])]
    y = df_ohe[dependent_variable]

    
    ### capture a list of columns that will be used for prediction
    global model_columns
    model_columns = list(x.columns)
    joblib.dump(model_columns, model_columns_file_path)

    
    
    ### define classifier
    global clf
    clf = RandomForestClassifier()
    
    start = time.time()
    clf.fit(x, y)
    print('> Fit in %.1f s' % (time.time() - start))
    

    ### evaluate model
    print('> Model training score: %s' % clf.score(x, y))

    
    ### serialize model
    joblib.dump(clf, model_file_path)

    return 'Success'


@app.route('/wipe', methods=['GET'])
def wipe():
    try:
        shutil.rmtree(MODEL_DIR)
        print(f"> Removed: {MODEL_DIR}")
        os.makedirs(MODEL_DIR)
        print(f"> Created: {MODEL_DIR}")
        return 'Model wiped'
    except Exception as e:
        print(str(e))
        return f'> Could not remove and recreate the directory: {MODEL_DIR}'


if __name__ == '__main__':
#    try:
#        port = int(sys.argv[1])
#    except Exception as e:
#        port = 80

    try:
        clf = joblib.load(model_file_path)
        print('> loaded: model')
        model_columns = joblib.load(model_columns_file_path)
        print('> loaded: model columns')
    except Exception as e:
        print('> No model here')
        print('> Train first')
        print(str(e))
        clf = None

    ### run the app
    app.run(
        debug=True
#        , host='0.0.0.0'
#        , port=port
    )
