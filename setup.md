# title  : my Titanic app with Python and Flask
# version: 2022-03-05T1510 AU
# review : 2022-03-06T2233 AU


* python vewrsion
```
python==3.7
```

* create app dir
```
> export APP_NAME={your text here}
> mkdir APP_NAME
> cd APP_NAME
```


* clone repository
```
> git clone https://gitlab.com/aleksandras.urbonas/titanic_flask_app.git
```


* install library `virtualenv`
```
> py -3.7 -m pip install virtualenv
```


* setup the virtual environment
```
> py -3.7 -m virtualenv venv
```
returns
```
created virtual environment CPython3.7.9.final.0-64 in 4275ms
  creator CPython3Windows(dest=C:\A\git\Titanic-Flask-App_AU\venv, clear=False, no_vcs_ignore=False, global=False)
  seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=C:\Users\Lenovo\AppData\Local\pypa\virtualenv)
    added seed packages: pip==22.0.3, setuptools==60.9.3, wheel==0.37.1
  activators BashActivator,BatchActivator,FishActivator,NushellActivator,PowerShellActivator,PythonActivator
```


* activate the created environment:
```
> venv\Scripts\activate.bat
```


* install libraries:
```
(venv) > py -m pip install -r req.txt
```


* run app:
```
(venv) > py app.py
```
